import Layout from '../components/Layout/Layout'

function App({ Component, pageProps, router: { pathname } }) {
  if (pathname.includes('login')) {
    return <Component {...pageProps} />
  }
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}



export default App