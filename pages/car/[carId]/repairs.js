import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

import Card from '../../../components/Card'
import http from '../../../services/http'

const CarsByClient = () => {
  const { query: { carId } } = useRouter();
  const [repairs, setRepairs] = useState([])

  useEffect(async () => {
    if (!carId) return;
    const { data } = await http({ method: 'get', url: `car/${carId}/repairs`, })
    setRepairs(data);
  }, [carId])

  return (
    <div>
      <h1>Reparaciones</h1>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Descripción</th>
            <th scope="col">Costo</th>
          </tr>
        </thead>
        <tbody>
          {repairs.map(repair => (
            <tr key={repair.id}>
              <th>{repair.description}</th>
              <td>{repair.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default CarsByClient
