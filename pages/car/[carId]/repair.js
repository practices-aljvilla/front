import React, { useState } from 'react'
import { Formik, ErrorMessage, Field } from 'formik';
import { useRouter } from 'next/router'

import http from '../../../services/http'

const validateForm = (values) => {
  const errors = {};
  if (!values.description) {
    errors.description = 'Requerido';
  }
  if (!values.price) {
    errors.price = 'Requerido';
  }
  return errors;
}

const NewCarRepair = () => {
  const [sendingForm, setSendingForm] = useState(false);
  const router = useRouter();
  const carId = router.query.carId;
  return (
    <div>
      <h2>Nuevo cliente</h2>
      <hr />
      <Formik
        initialValues={{
          description: '',
          price: '',
        }}
        validate={validateForm}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(false);
          setSendingForm(true);
          try {
            await http({ method: 'post', url: 'repairs', data: { ...values, carId } })
            setSendingForm(false);
            router.push(`/car/${carId}/repairs`)
          } catch (err) {
            console.log(err)
            setSendingForm(false);
          }

        }}
      >
        {({
          handleSubmit,
          isSubmitting,
        }) => {
          if (sendingForm) {
            return (<div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>)
          }
          return (
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Descripción</label>
                <Field type="text" name="description" className="form-control" />
                <ErrorMessage name="description" component="div" className="alert alert-danger" />
              </div>
              <div className="mb-3">
                <label htmlFor="price" className="form-label">Precio</label>
                <Field type="text" name="price" className="form-control" />
                <ErrorMessage name="price" component="div" className="alert alert-danger" />
              </div>

              <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                Guardar
              </button>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default NewCarRepair
