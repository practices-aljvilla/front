import React, { useState } from 'react'
import { Formik, ErrorMessage, Field } from 'formik'
import { useRouter } from 'next/router'
import axios from 'axios'
import getConfig from 'next/config';
import Cookies from 'js-cookie'

const { publicRuntimeConfig } = getConfig();
const axiosInstance = axios.create({
  baseURL: publicRuntimeConfig.backendUrl,
});

const validateForm = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = 'Email incorrecto';
  }
  if (!values.password) {
    errors.password = 'Requerido';
  }
  return errors;
}

const Login = () => {
  const [sendingForm, setSendingForm] = useState(false);
  const router = useRouter();
  return (
    <div className="container-fluid" >
      <div className="mx-auto my-5" style={{ width: '50%' }} >
        <h2>Inicio de sesión</h2>
        <hr />
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validate={validateForm}
          onSubmit={async (values, { setSubmitting }) => {
            setSubmitting(false);
            setSendingForm(true);
            try {
              const { data: { token } } = await axiosInstance({ method: 'post', url: 'user/login', data: values })
              console.log(token)
              Cookies.set('token', token)
              router.push('/client/list')
            } catch (err) {
              setSendingForm(false);
            }

          }}
        >
          {({
            handleSubmit,
            isSubmitting,
          }) => {
            if (sendingForm) {
              return (<div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>)
            }
            return (
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label">Nombre</label>
                  <Field type="email" name="email" className="form-control" />
                  <ErrorMessage name="email" component="div" className="alert alert-danger" />

                </div>
                <div className="mb-3">
                  <label htmlFor="password" className="form-label">Contraseña</label>
                  <Field type="password" name="password" className="form-control" />
                  <ErrorMessage name="password" component="div" className="alert alert-danger" />
                </div>

                <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                  Ingresar
              </button>
              </form>
            )
          }}
        </Formik>
      </div>
    </div>
  )
}

export default Login
