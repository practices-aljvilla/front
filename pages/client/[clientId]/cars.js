import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'

import http from '../../../services/http'

const CarContent = ({ car }) => (
  <div>
    <p><b>Modelo:</b> {car.model}</p>
    <p><b>Año:</b> {car.year}</p>
  </div>
)

const ClientCars = () => {
  const { query: { clientId } } = useRouter();
  const [cars, setCars] = useState([])

  useEffect(async () => {
    if (!clientId) return;
    const { data } = await http({ method: 'get', url: `client/${clientId}/cars` });
    setCars(data);
  }, [clientId])

  return (
    <div>
      <h1>Listado de autos</h1>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">Año</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {cars.map(car => (
            <tr key={car.id}>
              <th>{car.brand}</th>
              <td>{car.model}</td>
              <td>{car.year}</td>
              <td><Link href={`/car/${car.id}/repairs`}>Ver reparaciones</Link></td>
              <td><Link href={`/car/${car.id}/repair`}>Agregar reparación</Link></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default ClientCars
