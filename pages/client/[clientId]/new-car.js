import React, { useState } from 'react'
import { Formik, ErrorMessage, Field } from 'formik';
import { useRouter } from 'next/router'

import validateIdentificationValue from '../../../services/validateIdentificationValue'
import http from '../../../services/http'

const validateForm = (values) => {
  const errors = {};
  if (!values.brand) {
    errors.brand = 'Requerido';
  }
  if (!values.model) {
    errors.model = 'Requerido';
  }
  if (!values.year) {
    errors.year = 'Requerido';
  }
  return errors;
}

const NewCar = () => {
  const [sendingForm, setSendingForm] = useState(false);
  const router = useRouter();
  const clientId = router.query.clientId;
  return (
    <div>
      <h2>Nuevo cliente</h2>
      <hr />
      <Formik
        initialValues={{
          brand: '',
          model: '',
          year: '',
        }}
        validate={validateForm}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(false);
          setSendingForm(true);
          try {
            await http({ method: 'post', url: 'car', data: { ...values, clientId } })
            setSendingForm(false);
            router.push(`/client/${clientId}/cars`)
          } catch (err) {
            console.log(err)
            setSendingForm(false);
          }

        }}
      >
        {({
          handleSubmit,
          isSubmitting,
        }) => {
          if (sendingForm) {
            return (<div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>)
          }
          return (
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="brand" className="form-label">Marca</label>
                <Field type="text" name="brand" className="form-control" />
                <ErrorMessage name="brand" component="div" className="alert alert-danger" />
              </div>
              <div className="mb-3">
                <label htmlFor="model" className="form-label">Modelo</label>
                <Field type="text" name="model" className="form-control" />
                <ErrorMessage name="model" component="div" className="alert alert-danger" />
              </div>

              <div className="mb-3">
                <label htmlFor="year" className="form-label">Año</label>
                <Field type="text" name="year" className="form-control" />
                <ErrorMessage name="year" component="div" className="alert alert-danger" />
              </div>

              <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                Guardar
              </button>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default NewCar
