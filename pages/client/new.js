import React, { useState } from 'react'
import { Formik, ErrorMessage, Field } from 'formik';
import { useRouter } from 'next/router'

import validateIdentificationValue from '../../services/validateIdentificationValue'
import http from '../../services/http'

const validateForm = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = 'Email incorrecto';
  }
  if (!values.fullName) {
    errors.fullName = 'Requerido';
  }
  if (!values.phone) {
    errors.phone = 'Requerido';
  }
  if (!values.identificationValue) {
    errors.identificationValue = 'Requerido';
  } else if (!validateIdentificationValue(values.identificationValue)) {
    errors.identificationValue = 'R.U.N. Incorrecto'
  }
  return errors;
}

const NewClient = () => {
  const [sendingForm, setSendingForm] = useState(false);
  const router = useRouter();
  return (
    <div>
      <h2>Nuevo cliente</h2>
      <hr />
      <Formik
        initialValues={{
          fullName: '',
          identificationValue: '',
          phone: '',
          email: '',
        }}
        validate={validateForm}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(false);
          setSendingForm(true);
          try {
            await http({ method: 'post', url: 'client', data: values })
            setSendingForm(false);
            router.push('/client/list')
          } catch (err) {
            console.log(err)
            setSendingForm(false);
          }

        }}
      >
        {({
          handleSubmit,
          isSubmitting,
        }) => {
          if (sendingForm) {
            return (<div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>)
          }
          return (
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="fullName" className="form-label">Nombre</label>
                <Field type="text" name="fullName" className="form-control" />
                <ErrorMessage name="fullName" component="div" className="alert alert-danger" />

              </div>
              <div className="mb-3">
                <label htmlFor="identificationValue" className="form-label">R.U.N</label>
                <Field type="text" name="identificationValue" className="form-control" />
                <ErrorMessage name="identificationValue" component="div" className="alert alert-danger" />
              </div>

              <div className="mb-3">
                <label htmlFor="phone" className="form-label">Teléfono</label>
                <Field type="text" name="phone" className="form-control" />
                <ErrorMessage name="phone" component="div" className="alert alert-danger" />
              </div>

              <div className="mb-3">
                <label htmlFor="email" className="form-label">Email</label>
                <Field type="email" name="email" className="form-control" />
                <ErrorMessage name="email" component="div" className="alert alert-danger" />
              </div>

              <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                Guardar
              </button>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default NewClient
