import React, { useEffect, useState } from 'react'
import PrivateComponent from '../../components/PrivateComponent'
import ClientList from '../../components/ClientList'
import Pagination from '../../components/Pagination'
import http from '../../services/http'

const Clients = () => {
  const [clients, setClients] = useState({ docs: [], pages: 0, total: 0 })
  const [loading, setLoading] = useState(true)
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(async () => {
    try {
      setLoading(true)
      const { data } = await http({ method: 'get', url: `client?page=${currentPage}` });
      setClients(data);
      setLoading(false)
    } catch (err) {
      setLoading(false)
      console.log(err)
    }
  }, [currentPage])

  return (
    <div>
      <h1> Clientes </h1>
      <ClientList clients={clients} />
      {clients.total > 2 && <Pagination currentPage={currentPage} pages={clients.pages} previousAction={() => setCurrentPage(currentPage - 1)} nextAction={() => setCurrentPage(currentPage + 1)} />}
      {loading && <div className="spinner-border" role="status">
        <span className="visually-hidden">Loading...</span>
      </div>}
    </div>
  )
}

export default PrivateComponent(Clients)
