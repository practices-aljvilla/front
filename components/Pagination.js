import React from 'react'

const Pagination = ({ previousAction, nextAction, currentPage, pages }) => {
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        <li className="page-item">
          <button type="button" disabled={currentPage === 1} onClick={previousAction} className="page-link">Anterior</button>
        </li>
        <li className="page-item">
          <button type="button" disabled={currentPage >= pages} onClick={nextAction} className="page-link">Siguiente</button>
        </li>
      </ul>
    </nav>
  )
}

export default Pagination
