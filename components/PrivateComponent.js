import React from 'react'
import Router from "next/router";
import Cookies from 'js-cookie'

const WrappedComponent = (Component) => {
  const hocComponent = props => <Component {...props} />;
  const token = Cookies.get('token');

  hocComponent.getInitialProps = async context => {
    if (!token) {
      if (context.res) {
        context.res?.writeHead(302, {
          Location: '/login'
        });
        context.res?.end();
      } else {
        Router.replace(login);
      }
    }
    return { props: token };
  };
  return hocComponent;
};
export default WrappedComponent;