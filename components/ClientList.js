import React from 'react'
import Link from 'next/link'

const ClientList = ({ clients }) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">R.U.N</th>
          <th scope="col">Teléfono</th>
          <th scope="col"></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        {clients.docs.map(client => (
          <tr key={client.id}>
            <th>{client.fullName}</th>
            <td>{client.identificationValue}</td>
            <td>{client.phone}</td>
            <td><Link href={`/client/${client.id}/cars`}>Ver autos</Link></td>
            <td><Link href={`/client/${client.id}/new-car`}>Nuevo auto</Link></td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default ClientList
