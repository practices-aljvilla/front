import React from 'react'
import Link from 'next/link'

const Card = ({ title, content, path, pathTitle, children }) => {
  return (
    <div className="card" style={{ width: '18rem' }}>
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        {content && <p className="card-text">{content}</p>}
        {children}
        {(path && pathTitle) && <Link href={path} ><a className="btn btn-primary">{pathTitle}</a></Link>}
      </div>
    </div >
  )
}

export default Card
