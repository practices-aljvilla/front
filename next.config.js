module.exports = {
  publicRuntimeConfig: {
    // Will be available on both server and client
    backendUrl: 'http://localhost:3500/',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/client/list',
        permanent: true,
      },
    ]
  },
}