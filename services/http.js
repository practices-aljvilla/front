import axios from 'axios'
import getConfig from 'next/config';
import Cookies from 'js-cookie'

axios.interceptors
const { publicRuntimeConfig } = getConfig();
const instance = axios.create({
  baseURL: publicRuntimeConfig.backendUrl,
});
instance.interceptors.request.use(
  (config) => {
    const token = Cookies.get('token');
    config.headers.Authorization = token;
    return config;
  },
  (error) => error,
);


instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    return Promise.reject(err);
  },
);
export default instance;