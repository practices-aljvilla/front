const validate = (value) => {
  const rut = String(value)
    .toUpperCase()
    .replace(/[.]|[-]/g, '');
  function dv(T) {
    let M = 0;
    let S = 1;
    for (; T; T = Math.floor(T / 10)) {
      S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
    }
    return S ? S - 1 : 'K';
  }
  const producedDV = dv(rut.substr(0, rut.length - 1));
  const requestDV = rut.substr(-1);
  return String(producedDV) === String(requestDV);
};

export default validate;