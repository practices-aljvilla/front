
This README would normally document whatever steps are necessary to get the application up and running.

  

Things you may want to cover:

  

## Requirements

  

- node v14

- git

  

## First run

  

Note: Only tested on Mac OS X and Linux so far.

  

Assuming you have a installed all requirements:

  

```

$ git clone https://aljvilla@bitbucket.org/practices-aljvilla/front.git

```

```

$ cd front

```

```

$ npm i

```

```

$ npm run dev

```

  

It will:

After it finishes you will see something like this on your console:

```
ready - started server on 0.0.0.0:3000, url: http://localhost:3000
event - compiled successfully
event - build page: /next/dist/pages/_error
wait - compiling...
event - compiled successfully
```
To login with default user:
user: workshop@workshop.com
password: 1234